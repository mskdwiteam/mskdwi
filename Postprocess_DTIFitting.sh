# Madison, WI. 08/31/2016. 1:36 p.m.

# 08/30/2016. 3:22 p.m.
# Will do this after eddy is complete.

baseRoot=/home/fang/D/Data/DTI/1127_08132016_WarpCoil/009_enhancedFatSat
outputRoot=/home/fang/D/Data/DTI/1127_08132016_WarpCoil/009_enhancedFatSat_Postprocessed

bvalFile=${baseRoot}/20160813_165612s009a1001.bval
schemeFile=${outputRoot}/20160813_165612s009a1001.scheme
maskFile=${baseRoot}/20160813_165612s009a1001_brain_mask.nii.gz

prefix1=${outputRoot}/dwiECC
prefix2=${outputRoot}/DTI

bvecFile=${prefix1}.eddy_rotated_bvecs

if [[ ! -f "${prefix1}.Bfloat" ]]; then
	echo "${prefix1}.Bfloat not found."
	fsl2scheme -flipy -bvalfile ${bvalFile} -bvecfile ${bvecFile} > ${schemeFile}
	image2voxel -4dimage ${prefix1}.nii.gz -inputdatatype float -outputdatatype float -outputfile ${prefix1}.Bfloat
	modelfit -inputfile ${prefix1}.Bfloat -inputdatatype float -schemefile ${schemeFile} -outputdatatype float -model nldt_pos -outputfile ${prefix2}.Bfloat -bgmask ${maskFile}
	dt2nii -inputfile ${prefix2}.Bfloat -inputdatatype float -header ${prefix1}.nii.gz -gzip -outputroot ${prefix2}_
fi

for PROG in fa md; do
	cat ${prefix2}.Bfloat | ${PROG} -inputdatatype float -outputdatatype float | voxel2image -inputdatatype float -outputroot ${outputRoot}/${PROG} -header ${prefix1}.nii.gz
done

cat ${prefix2}.Bfloat | dteig -inputdatatype float -inputmodel dt -outputdatatype float > ${prefix2}_eig.Bfloat


# 08/31/2016. 12:43 p.m.
echo "Sanity checks."
pdview -inputfile ${prefix2}_eig.Bfloat -inputmodel dteig -header ${prefix1}.nii.gz -inputdatatype float

