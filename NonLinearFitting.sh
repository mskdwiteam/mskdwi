#!/bin/bash

fsl2scheme -bvalfile bval.txt -bvecfile bvec.txt > acq.scheme
image2voxel -4dimage dwi.nii.gz -inputdatatype float -outputdatatype float -outputfile dwi.Bfloat

echo "Residual analysis using constrained non-linear estimation."
modelfit -inputfile dwi.Bfloat -inputdatatype float -schemefile acq.scheme -outputdatatype float -model nldt_pos -outputfile dti.Bfloat -bgmask mask.nii.gz
datasynth -inputfile dti.Bfloat -inputdatatype float -inputmodel dt -schemefile acq.scheme > synthdwi.Bfloat
nvols=$(fslval dwi.nii.gz dim4)
voxel2image -inputfile synthdwi.Bfloat -inputdatatype float -outputroot synthdwi -header dwi.nii.gz -gzip -components ${nvols}

fslmerge -t synthdwi.nii.gz synthdwi??*.nii.gz
fslmaths dwi.nii.gz -sub synthdwi.nii.gz resid.nii.gz
fslmaths resid.nii.gz -sqr residsqr.nii.gz
sigma=$(fslstats ${prefix}_resid_sqr.nii.gz -k ${mask} -P 50)
sigma=$(echo "scale=3;sqrt($sigma)"|bc)
echo "Noise variance using CNL estimation ${sigma}."
rm synthdwi??*.nii.gz -f

echo "Residual analysis using linear estimation."
modelfit -inputfile dwi.Bfloat -inputdatatype float -schemefile acq.scheme -outputdatatype float -model ldt -outputfile dti.Bfloat -bgmask mask.nii.gz
datasynth -inputfile dti.Bfloat -inputdatatype float -inputmodel dt -schemefile acq.scheme > synthdwi.Bfloat
voxel2image -inputfile synthdwi.Bfloat -inputdatatype float -outputroot synthdwi -header dwi.nii.gz -gzip -components ${nvols}

fslmerge -t synthdwi.nii.gz synthdwi??*.nii.gz
fslmaths dwi.nii.gz -sub synthdwi.nii.gz resid.nii.gz
fslmaths resid.nii.gz -sqr residsqr.nii.gz
sigma=$(fslstats ${prefix}_resid_sqr.nii.gz -k ${mask} -P 50)
sigma=$(echo "scale=3;sqrt($sigma)"|bc)
echo "Noise variance using linear estimation ${sigma}."
rm synthdwi??*.nii.gz -f
