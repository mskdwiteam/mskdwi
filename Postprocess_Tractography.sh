# Basic tractography setup.
# At WIMR.
# baseRoot=/home/fang/D/Data/DTI/1127_08132016_WarpCoil/009_enhancedFatSat
# outputRoot=/home/fang/D/Data/DTI/1127_08132016_WarpCoil/009_enhancedFatSat_Postprocessed

# At Waisman.
baseRoot=/scratch/Nagesh/MSKDWI
outputRoot=${baseRoot}

maskFile=${baseRoot}/20160813_165612s009a1001_brain_mask.nii.gz
prefix2=${outputRoot}/DTI

track -inputmodel dt -inputfile ${prefix2}.Bfloat -inputdatatype float -seedfile ${maskFile} -anisthresh 0 -curvethresh 45 -curveinterval 0.5 -stepsize 0.1 -outputdatatype float -tracker euler -outputfile ${prefix2}TractsLightWeightVis.Bfloat
minLength=100
cat ${prefix2}TractsLightWeightVis.Bfloat | procstreamlines -mintractlength ${minLength} -outputfile ${prefix2}TractsLightWeightVisFiltered.Bfloat -seedfile ${maskFile}
camino_to_trackvis -i ${prefix2}TractsLightWeightVisFiltered.Bfloat -o /scratch.local/TractsLightWeightVisFiltered.trk --nifti ${maskFile} --phys-coords

