# SCRIPT Preprocess_TOPUP.sh
#
# Script to preprocess PE blip-reversed (AP/PA) thigh muscle diffusion
# data using TOPUP (distortion correction), EDDY (eddy current correction),
# and DTIFIT (tensor fitting). Data acquired from GE Signa Premier with
# multiband (HB) acceleration and no parallel imaging acceleration.
#
# Requires access to scripts: *.waisman.wisc.edu:~shurley/Prog/DiffPreProc
#
# Samuel A. Hurley
# University of Wisconsin
# 27-Jun-2017

# Resample data to 2x2x3 resolution for processing

# Create new header with target resolution
fslcreatehd 128 128 80 19 2.96875 2.96875 3 4.137 0 0 0 4 REF_HDR

# Resample series
flirt -in ../IMG_12_lwrAx_DWI_TENSOR_B-500_15directions_Hyperband -out IMG_12_lwrAx_DWI_B500_15_HB -ref REF_HDR -applyxfm -init I4.xfm -interp sinc
flirt -in ../IMG_13_lwrAx_DWI_Tens_B-500_15dir_PE+_Hyperband -out IMG_13_lwrAx_DWI_B500_15_PE+_HB -ref REF_HDR -applyxfm -init I4.xfm -interp sinc

# Copy bvals and bvecs
cp ../IMG_12_lwrAx_DWI_TENSOR_B-500_15directions_Hyperband.bval ./IMG_12_lwrAx_DWI_B500_15_HB.bval
cp ../IMG_12_lwrAx_DWI_TENSOR_B-500_15directions_Hyperband.bvec ./IMG_12_lwrAx_DWI_B500_15_HB.bvec
cp ../IMG_13_lwrAx_DWI_Tens_B-500_15dir_PE+_Hyperband.bval ./IMG_13_lwrAx_DWI_B500_15_PE+_HB.bval
cp ../IMG_13_lwrAx_DWI_Tens_B-500_15dir_PE+_Hyperband.bvec ./IMG_13_lwrAx_DWI_B500_15_PE+_HB.bvec

# Add diffusion preproc to path
export PATH=~shurley/Prog/DiffPreproc/:${PATH}

# Run Stam's diffusion preprocessing script
# Echo spacing is 0.552
DiffPreproc.sh IMG_12_lwrAx_DWI_B500_15_HB IMG_13_lwrAx_DWI_B500_15_PE+_HB . 0.552 2 1 1
